train <- read.csv(file="c:/train.csv", header=TRUE, sep=",")
test <- read.csv(file="c:/test.csv", header=TRUE, sep=",")

summary(train)
str(train)
#check if there are any na's
any(is.na(train))

#separate date time to date and time
#datetime->character:
train$datetime <- as.character(train$datetime)
str(train)

#split date and time by blank
train$date <-sapply(strsplit(train$datetime,' '), "[", 1)
train$date <- as.Date(train$date)

#split time to hours by blank
train$time <-sapply(strsplit(train$datetime,' '), "[", 2)
train$hour <-sapply(strsplit(train$time,':'), "[", 1)
train$hour <- as.numeric(train$hour)

#remove time
train$time <- NULL

#turn season into factort
train$season <- factor(train$season,
                       levels = c(1,2,3,4),
                       labels = c("spring", "summer", "fall", "winter"))

library(ggplot2)

#how temperature affects count
pl <- ggplot(train, aes(temp,count)) + geom_point(alpha = 0.3, aes(color = temp))

#how date affects count
pl1 <- ggplot(train, aes(date,count)) + geom_point(alpha = 0.5, aes(color = temp))
pl1 + scale_color_continuous(low = 'blue', high = 'red')

#how season affects count
pl2 <- ggplot(train, aes(season,count)) + geom_boxplot(aes(color=season))
#another way//; boxplot(count~season,data=train, main="Count fer season", xlab="Season", ylab="Count")

#how hour affects count on weekdays
pl3 <- ggplot(train[train$workingday != 1,], aes(hour,count))
pl3 <- pl3 + geom_point(position = position_jitter(w=0.5,h=0), aes(color=temp))

#how hour affects count on sundays
pl4 <- ggplot(train[train$workingday != 0,], aes(hour,count))
pl4 <- pl4 + geom_point(position = position_jitter(w=0.5,h=0), aes(color=temp))

#remove datetime field
train$datetime <- NULL

#split into train and test
splitDate <- as.Date('2012-05-01')
dateFilter <- train$date <= splitDate 
train.train <-train[dateFilter,]
train.test <- train[!dateFilter,]


#linear regresion
model <- lm(count~ . -atemp -casual -registered -date, train.train)
summary(model)

#prediction for train and test
predicted.train <- predict(model, train.train)
predicted.test <- predict(model, train.test)

MSE.train = mean((train.train$count-predicted.train)**2)
MSE.test = mean((train.test$count-predicted.test)**2)

#check overfitting
RMSE.train <- MSE.train**0.5
RMSE.test <- MSE.test**0.5
summary(train)


str(train)
