library(ISLR)

df <- College
str(df)

#Plot Room.Board against Grad.Rate while color is Private what is the conclusion?
---------------------------------------------------------------------------------
library(ggplot2)

pl3 <- ggplot(df, aes(x = Room.Board, y= Grad.Rate))
pl3 <- pl3 + geom_point(aes(color = Private, size = 5, alpha = 0.5))


#Plot a histogram of Grad.Rate and show how Private is distributed in each bar what is the conclusion? 
---------------------------------------------------------------------------------
pl <- ggplot(df, aes(x = Grad.Rate))
pl <- pl + geom_histogram(binwidth = 4, aes(fill = Private), color = 'black')

  
#Plot a histogram of F.Undergrad and show how Private is distributed in each bar what is the conclusion?  
---------------------------------------------------------------------------------
pl2 <- ggplot(df, aes(x = F.Undergrad))
pl2 <- pl2 + geom_histogram(aes(fill = Private),color = 'black')

  
#Fix the problem of schools with Grad.Rate above 100%
---------------------------------------------------------------------------------
str(df)
summary(df)

conv_yesno <- function(x){
  x <- ifelse(x>0,1,0)
  x <- factor(x,levels = c(1,0), labels = c('Yes', 'No'))
}

train <- apply(train_dtm, MARGIN = 1:2,conv_yesno)

greater100 <- function(x){
  if(x>100){
    return(100)
  }else{
    return (x)
  }
}

df$Grad.Rate <- sapply(df$Grad.Rate, greater100)

#Divide the data into train set and test set 
---------------------------------------------------------------------------------
library(caTools)
set.seed(101)
sample <- sample.split(df,0.7)

train <- subset(df, sample)
test <- subset(df, !sample)


#Derive the tree model 
---------------------------------------------------------------------------------
library(rpart)
library(rpart.plot)

model <-rpart(Private ~ .,train)
  
#Plot the model 
---------------------------------------------------------------------------------
prp(model)

#Compute confusion matrix, Recall and Precision
---------------------------------------------------------------------------------
library(SDMTools)

#predict
predict <- predict(model,test)
head(predict)
str(predict)
#predict 0/1
predict01 <- ifelse(predict>0.5,1,0)
predict01 <- as.data.frame(predict01)
predict01$Yes
#actual 0/1
#turn string into 0/1 (1= yes, 0=no) for actual results
conv_10 <-function(x){
  return (ifelse(x=='Yes',1,0))
}
actual10 <-sapply(test$Private,conv_10)

#creating confusion matrix
confusion.df <- confusion.matrix(actual10,predict01$Yes)

#giving name to each field of the matrix
TP <- confusion.df[2,2]
FP <- confusion.df[2,1]
TN <- confusion.df[1,1]
FN <- confusion.df[1,2]

recall <- TP/(TP+FN) 
precision <- TP/(TP+FP)








