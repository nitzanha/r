train
str(train)

train$datetime <- as.character(train$datetime)
str(train)

train$date <-sapply(strsplit(train$datetime,' '), "[", 1)
train$date <- as.Date(train$date)

train$time <-sapply(strsplit(train$datetime,' '), "[", 2)

train$hour <-sapply(strsplit(train$time,':'), "[", 1)
train$hour <- as.numeric(train$hour)


#Q1 A chart showing how temperature affects the count
---------------------------
pl1 <- ggplot(train, aes(x = temp, y = count))
pl1 <- pl1 + geom_point(alpha = 0.2)
#between 20 to 30 C the demand for bikes was high
#between 0-10 C the demand is low


#Q2 A chart showing how date affects count
---------------------------
pl2 <- ggplot(train, aes(x = date, y = count))
pl2 <- pl2 + geom_point(alpha = 0.2)
# November-May the dement was low,while June to October the demend was high
# 2011 was lower than 2012

#Q3 A chart showing how season affects count (use here a boxplot) learn here about what a boxplot is and how to interpret it (turn season into factor first, add labels to season (hint - the factor function has a label parameter)
---------------------------
train$season <- factor(train$season,
                    levels = c(1,2,3,4),
                    labels = c("spring", "summer", "fall", "winter"))
str(train)
pl3 <- boxplot(count~season,data=train, main="Count fer season", xlab="Season", ylab="Count")
#Seasons summer and fall had the most demand

  
#Q4 A chart showing how hour AND temperature together affects count. Use x axis for hour and color for temperature. Split the chart to weekdays and sunday. (bonus - learn how to use ggplot to show these two charts on the same page)
---------------------------
pl4 <- ggplot(train, aes(x=hour, y = count))
pl4 <- pl4 + geom_point(aes(color = temp))
#the "rush hours" are 8AM, 5,6PM


#Q5 A chart showing anything you like so long as it provides an interesting insight into the data 
---------------------------                                                                                                                          
train$holiday <- factor(train$holiday,
                         levels = c(0,1),
                         labels = c("regular_day", "holiday"))
str(train)
pl5 <- ggplot(train, aes(x=holiday, y = count))
pl5 <- pl5 + geom_bin2d() + scale_fill_gradient(low = 'green', high = 'red')
#most of the days counted were regular days. In this days there were not much of a demand.

