#random fores

install.packages('randomForest')
library(randomForest)
library(rpart)
str(kyphosis)

#runing the random forest model
rf.model <- randomForest(Kyphosis ~ . , data= kyphosis)
print(rf.model)  #gives MSE and confusion matrix

#predict (should be with test set)
predicted <- predict(rf.model,kyphosis)

#see the probability
predictedprob <- predict(rf.model,kyphosis, type ='prob')
#see only present records
predictedprob[,'present']

?randomForest
 

#roc curv
install.packages('pROC')
library(pROC)

#building roc chart
rocCurve <- roc(kyphosis$Kyphosis, predictedprob[,'present'], levels = c("absent","present"))
plot(rocCurve, col="yellow", main='ROC chart')